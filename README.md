# carros_brasil

> SPA desenvolvida em Vue.js. 

## Objetivo do projeto

> - Realizar consulta da tabela FIPE.
> - Navegar na galeria de fotos dos primeiros carros populares no Brasil.
> - Navegar em histórias dos primeiros carros no Brasil.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
