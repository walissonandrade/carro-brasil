import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/feature/Inicio'
import Consulta from '@/feature/Consulta'
import Historia from '@/feature/Historia'
import Galeria from '@/feature/Galeria'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: 'consulta',
      name: 'Consulta',
      component: Consulta
    },
    {
      path: 'historia',
      name: 'Historia',
      component: Historia
    },
    {
      path: 'galeria',
      name: 'Galeria',
      component: Galeria
    }
  ]
})
